# First Time

```
odoo-openupgrade-wizard init --initial-version 12.0 --final-version 16.0 --project-name grap-12-16 --extra-repository OCA/web,OCA/server-tools
```

# Each time (image changed)

```
odoo-openupgrade-wizard docker-build
```

Each time (code changed)

```
odoo-openupgrade-wizard pull-submodule -v 12.0
odoo-openupgrade-wizard pull-submodule -v 16.0
odoo-openupgrade-wizard get-code
odoo-openupgrade-wizard estimate-workload
```

# Run Upgrade (demo database)

```
odoo-openupgrade-wizard install-from-csv --database test_grap_12_16_001
odoo-openupgrade-wizard upgrade --database test_grap_12_16_001
```

# Run Upgrade (CAAP Database)

```
oow restoredb\
  --database caap_2023_07_06\
  --database-path=./data_from_production/caap_production__2023_07_06__03_20_02.dump\
  --database-format=c\
  --filestore-path=./data_from_production/caap_production__2023_07_06__03_20_02.tar.gz\
  --filestore-format=t
```

oow upgrade --database=caap_2023_07_06

# optional

odoo-openupgrade-wizard run --step 1 --database test_migration_12_15\_\_xxx
odoo-openupgrade-wizard run --step 1 --database test_grap_12_16_012

odoo-openupgrade-wizard upgrade --database test_grap_12_16_012

# Enter in psql

docker exec -it grap-12-16-container-postgres psql --username=odoo --dbname=test_grap_12_16_012

Configuring postgres https://severalnines.com/blog/architecture-and-tuning-memory-postgresql-databases/

`Shared_buffers` : 128MB --> 4GO
(25% de 16GO)

`work_mem` : 4MB --> 64MB

`maintenance_work_mem` : 64MB --> 1638MB
(10% de 16GO)

`Effective_cache_size` : 4GO -> 8GO
(50% de 16GO)

https://dba.stackexchange.com/questions/103148/is-it-possible-to-run-postgres-with-no-wal-files-being-produced

`wal_level` replica --> minimal
`fsync` on --> off
`full_page_writes` on --> off
`synchronous_commit` on --> off

docker run --name grap-12-16-container-postgres\
 --volume /home/sylvain/grap_dev/migrations/odoo-openupgrade-wizard_grap_12_16:/env/\
 --volume grap-12-16-volume-postgres:/var/lib/postgresql/data/pgdata/\
 --volume "\$PWD/custom-postgres.conf":/etc/postgresql/postgresql.conf\
 --env POSTGRES_USER=odoo\
 --env POSTGRES_PASSWORD=odoo\
 --env POSTGRES_DB=postgres\
 --detach\
 --env PGDATA=/var/lib/postgresql/data/pgdata postgres:13\
 -c 'config_file=/etc/postgresql/postgresql.conf'

# TODO Migrate to 16.0

- oca/account-financial-tools/account_subsequence_fiscal_year

- oca/account-invoicing/account_invoice_supplierinfo_update
- oca/account-invoicing/account_invoice_supplierinfo_update_discount
- oca/account-invoicing/account_invoice_supplierinfo_update_triple_discount

- oca/geospatial/base_geolocalize_company
- oca/geospatial/web_view_leaflet_map

- oca/margin-analysis/product_margin_classification
  https://github.com/OCA/product-attribute/pull/1114#pullrequestreview-1044796593
- Dev : product_pricelist_simulation_margin

- oca/pos/pos_cash_control_multiple_config
- oca/pos/pos_hide_banknote_button
- oca/pos/pos_hide_empty_category
- oca/pos/pos_journal_image
- oca/pos/pos_margin_account_invoice_margin
- oca/pos/pos_meal_voucher
- oca/pos/pos_multiple_control
- oca/pos/pos_order_line_no_unlink

- oca/pos/pos_picking_load ou https://github.com/OCA/pos/pull/841 ?

- oca/pos/pos_price_to_weight
- oca/pos/pos_pricelist_technical
- oca/pos/pos_tare
- oca/pos/pos_to_weight_by_product_uom

- oca/product-attribute/product_pricelist_direct_print # Important changes in V12
- oca/product-attribute/product_pricelist_simulation # Important changes in v12
- oca/product-attribute/product_uom_po_domain

- QD : ? oca/purchase-workflow/purchase_no_rfq

- QD : ? oca/stock-logistics-reporting/stock_picking_report_summary

- OCA/web/web_responsive_company
