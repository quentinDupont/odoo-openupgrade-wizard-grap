#!/bin/bash

# # Enter in the virtualenv
# . ../odoo-openupgrade-wizard/env/bin/activate

# cd src/env_12.0/repo_submodule/
# git pull origin 12.0
# cd ../../../
cd src/env_16.0/repo_submodule/
git pull origin 16.0
cd ../../../

oow get-code --versions=16.0

oow estimate-workload
