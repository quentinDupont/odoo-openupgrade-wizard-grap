
odoo-openupgrade-wizard init\
    --initial-release 12.0\
    --final-release 15.0\
    --project-name my-customer-12-15\
    --extra-repository OCA/web,OCA/server-tools

odoo-openupgrade-wizard docker-build
